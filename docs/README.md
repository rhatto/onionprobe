# Onionprobe

![](assets/logo.jpg "Onionprobe")

[Onionprobe][] is a tool for testing and monitoring the status of
[Tor Onion Services](https://community.torproject.org/onion-services/) sites.

It can run a single time or continuously to probe a set of onion services
endpoints and paths, optionally exporting to
[Prometheus](https://prometheus.io) and with [Grafana](https://grafana.com/)
and [Alertmanager](https://github.com/prometheus/alertmanager) support.

[Onionprobe]: https://gitlab.torproject.org/tpo/onion-services/onionprobe
